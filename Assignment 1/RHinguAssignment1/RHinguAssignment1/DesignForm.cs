﻿/*
 * DesignForm.cs
 * Assignment1
 * Designing Game
 * Raj Hingu
 * 2019-09-29
 * 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;



namespace RHinguAssignment1
{   /// <summary>
/// A simple class that designs a Game
/// </summary>
    public partial class DesignForm : Form
    {
        public DesignForm()
        {
            InitializeComponent();
        }

        // Declaring Class Variables and Constants
        const int LEFT = 300;
        const int TOP = 150;
        const int WIDTH = 60;
        const int HEIGHT = 60;
        const int VGAP = 0;

        // Declaring enum
        enum ToolBox
        {
            None,
            Hero,
            Wall,
            Box,
            Destination
        }

        ToolBox currentTool = ToolBox.None;


        private void DesignForm_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// This method executes when Generate Button is clicked and picture boxes are formed according to the input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Generate_Click(object sender, EventArgs e)
        {

            try
            {
                // Converting Text to integer
                int numberOfRows = int.Parse(txt_rows.Text);
                int numberOfColumns = int.Parse(txt_columns.Text);

                if (numberOfColumns<0 || numberOfRows<0)
                {
                    MessageBox.Show("Rows and Columns should be greater than 0", "Sokoban",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }

                int x = LEFT;
                int y = TOP;
                
                //Using for loop for generating pictureboxes in a unifrom manner
                for (int i = 0; i < numberOfColumns; i++)
                {

                    x = 300;

                    for (int j = 0; j < numberOfRows; j++)
                    {
                        PictureBox p = new PictureBox();


                        p.Left = x;
                        p.Top = y;
                        p.Width = WIDTH;
                        p.Height = HEIGHT;
                        p.Text = i.ToString();
                        p.BorderStyle = BorderStyle.Fixed3D;
                        this.Controls.Add(p);
                        p.Click += picBox_Click;

                        x += WIDTH;

                    }

                    y += HEIGHT;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in Rows and Columns " + ex.Message, "Sokoban", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }

        }
        /// <summary>
        /// This method selects the current tool as empty when None Button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_none_Click(object sender, EventArgs e)
        {
            currentTool = ToolBox.None;
        }
        /// <summary>
        /// This method selects the current tool as a Hero when Hero Button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_hero_Click(object sender, EventArgs e)
        {
            currentTool = ToolBox.Hero;
        }

        /// <summary>
        /// This method selects the current tool as a Wall when Wall Button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_wall_Click(object sender, EventArgs e)
        {
            currentTool = ToolBox.Wall;
        }

        /// <summary>
        /// This method selects the current tool as a Box when Box Button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_box_Click(object sender, EventArgs e)
        {
            currentTool = ToolBox.Box;
        }

        /// <summary>
        /// This method selects the current tool as a Destination when Destination Button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_destination_Click(object sender, EventArgs e)
        {
            currentTool = ToolBox.Destination;
        }

        /// <summary>
        /// This method replaces or inserts the selected image to the generated picture box 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBox_Click(object sender, EventArgs e)
        {
            PictureBox p = (PictureBox)sender;
            p.SizeMode = PictureBoxSizeMode.Zoom;
            // using switch case for selecting the images
            switch (currentTool)
            {
                case ToolBox.None:
                   p.Image = null ;
                    break;
                case ToolBox.Hero:
                    p.Image = RHinguAssignment1.Properties.Resources.Hero;

                    break;
                case ToolBox.Wall:
                    p.Image = RHinguAssignment1.Properties.Resources.Wall;
                    break;
                case ToolBox.Box:
                    p.Image = RHinguAssignment1.Properties.Resources.Box;
                    break;
                case ToolBox.Destination:
                    p.Image = RHinguAssignment1.Properties.Resources.Circle;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// This method is used to write the inputs to the file.
        /// </summary>
        /// <param name="fileName"></param>
        private void save(string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);
            int rows = int.Parse(txt_rows.Text);
            int columns = int.Parse(txt_columns.Text);

            writer.WriteLine(rows);
            writer.WriteLine(columns);

            
            writer.Close();

        }
        /// <summary>
        /// this method is used to save the file .
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
       
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            

          
            DialogResult r = saveFileDialog1.ShowDialog();

            switch (r)
            {
                case DialogResult.None:
                    break;
                case DialogResult.OK:
                    try
                    {
                        string fileName = saveFileDialog1.FileName;
                        save(fileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error in file save" + ex.Message);
                    }
                    break;
                case DialogResult.Cancel:
                    break;
                case DialogResult.Abort:
                    break;
                case DialogResult.Retry:
                    break;
                case DialogResult.Ignore:
                    break;
                case DialogResult.Yes:
                    break;
                case DialogResult.No:
                    break;
                default:
                    break;
            }



          

            MessageBox.Show("Saved Successfully","Sokoban",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        /// <summary>
        /// This method is for exiting the Design Tab when exit is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
