﻿namespace RHinguAssignment1
{
    partial class DesignForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignForm));
            this.label1 = new System.Windows.Forms.Label();
            this.txt_rows = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_columns = new System.Windows.Forms.TextBox();
            this.btn_Generate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_none = new System.Windows.Forms.Button();
            this.btn_hero = new System.Windows.Forms.Button();
            this.btn_wall = new System.Windows.Forms.Button();
            this.btn_box = new System.Windows.Forms.Button();
            this.btn_destination = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.img_buttons = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rows:";
            // 
            // txt_rows
            // 
            this.txt_rows.Location = new System.Drawing.Point(126, 36);
            this.txt_rows.Name = "txt_rows";
            this.txt_rows.Size = new System.Drawing.Size(121, 20);
            this.txt_rows.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(352, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Columns:";
            // 
            // txt_columns
            // 
            this.txt_columns.Location = new System.Drawing.Point(440, 35);
            this.txt_columns.Name = "txt_columns";
            this.txt_columns.Size = new System.Drawing.Size(121, 20);
            this.txt_columns.TabIndex = 3;
            // 
            // btn_Generate
            // 
            this.btn_Generate.Location = new System.Drawing.Point(655, 36);
            this.btn_Generate.Name = "btn_Generate";
            this.btn_Generate.Size = new System.Drawing.Size(75, 23);
            this.btn_Generate.TabIndex = 4;
            this.btn_Generate.Text = "Generate";
            this.btn_Generate.UseVisualStyleBackColor = true;
            this.btn_Generate.Click += new System.EventHandler(this.btn_Generate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "ToolBox";
            // 
            // btn_none
            // 
            this.btn_none.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_none.ImageIndex = 4;
            this.btn_none.ImageList = this.img_buttons;
            this.btn_none.Location = new System.Drawing.Point(37, 148);
            this.btn_none.Name = "btn_none";
            this.btn_none.Size = new System.Drawing.Size(102, 62);
            this.btn_none.TabIndex = 6;
            this.btn_none.Text = "None";
            this.btn_none.UseVisualStyleBackColor = true;
            this.btn_none.Click += new System.EventHandler(this.btn_none_Click);
            // 
            // btn_hero
            // 
            this.btn_hero.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_hero.ImageIndex = 3;
            this.btn_hero.ImageList = this.img_buttons;
            this.btn_hero.Location = new System.Drawing.Point(37, 235);
            this.btn_hero.Name = "btn_hero";
            this.btn_hero.Size = new System.Drawing.Size(102, 62);
            this.btn_hero.TabIndex = 7;
            this.btn_hero.Text = "Hero";
            this.btn_hero.UseVisualStyleBackColor = true;
            this.btn_hero.Click += new System.EventHandler(this.btn_hero_Click);
            // 
            // btn_wall
            // 
            this.btn_wall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_wall.ImageIndex = 0;
            this.btn_wall.ImageList = this.img_buttons;
            this.btn_wall.Location = new System.Drawing.Point(37, 329);
            this.btn_wall.Name = "btn_wall";
            this.btn_wall.Size = new System.Drawing.Size(102, 62);
            this.btn_wall.TabIndex = 8;
            this.btn_wall.Text = "Wall";
            this.btn_wall.UseVisualStyleBackColor = true;
            this.btn_wall.Click += new System.EventHandler(this.btn_wall_Click);
            // 
            // btn_box
            // 
            this.btn_box.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_box.ImageIndex = 1;
            this.btn_box.ImageList = this.img_buttons;
            this.btn_box.Location = new System.Drawing.Point(37, 426);
            this.btn_box.Name = "btn_box";
            this.btn_box.Size = new System.Drawing.Size(102, 62);
            this.btn_box.TabIndex = 9;
            this.btn_box.Text = "Box";
            this.btn_box.UseVisualStyleBackColor = true;
            this.btn_box.Click += new System.EventHandler(this.btn_box_Click);
            // 
            // btn_destination
            // 
            this.btn_destination.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_destination.ImageIndex = 2;
            this.btn_destination.ImageList = this.img_buttons;
            this.btn_destination.Location = new System.Drawing.Point(37, 531);
            this.btn_destination.Name = "btn_destination";
            this.btn_destination.Size = new System.Drawing.Size(102, 62);
            this.btn_destination.TabIndex = 10;
            this.btn_destination.Text = "Destination";
            this.btn_destination.UseVisualStyleBackColor = true;
            this.btn_destination.Click += new System.EventHandler(this.btn_destination_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1146, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // img_buttons
            // 
            this.img_buttons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("img_buttons.ImageStream")));
            this.img_buttons.TransparentColor = System.Drawing.Color.Transparent;
            this.img_buttons.Images.SetKeyName(0, "Wall.jpg");
            this.img_buttons.Images.SetKeyName(1, "Box.png");
            this.img_buttons.Images.SetKeyName(2, "Circle.png");
            this.img_buttons.Images.SetKeyName(3, "Hero.png");
            this.img_buttons.Images.SetKeyName(4, "None.png");
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(986, 83);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // DesignForm
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 749);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_destination);
            this.Controls.Add(this.btn_box);
            this.Controls.Add(this.btn_wall);
            this.Controls.Add(this.btn_hero);
            this.Controls.Add(this.btn_none);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_Generate);
            this.Controls.Add(this.txt_columns);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_rows);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "DesignForm";
            this.Text = "Design Form";
            this.Load += new System.EventHandler(this.DesignForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_rows;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_columns;
        private System.Windows.Forms.Button btn_Generate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_none;
        private System.Windows.Forms.Button btn_hero;
        private System.Windows.Forms.Button btn_wall;
        private System.Windows.Forms.Button btn_box;
        private System.Windows.Forms.Button btn_destination;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ImageList img_buttons;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}