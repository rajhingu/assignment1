﻿/*
 * SokobanControlPanel.cs
 * Assignment1
 * Designing Game
 * Raj Hingu
 * 2019-09-29
 * 
 */

 /*
  * 
  *  Sokoban Control Panel 
  * 
  * 
  * 
  * 
  * 
  * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace RHinguAssignment1
{
    /// <summary>
    /// A simple class that connects one form to another
    /// </summary>
    public partial class SokobanControlPanel : Form
    {
        /// <summary>
        /// Default Constructor 
        /// </summary>
        public SokobanControlPanel()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This method takes to the design form when clicked on Design. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_design_Click(object sender, EventArgs e)
        {
            DesignForm d = new DesignForm();
            d.ShowDialog();
        }
        /// <summary>
        /// This method closes the Program when clicked on exit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
